import Axios from 'axios'

export default new function () {
  const apiPrefix = '/api/'

  function handleError (status, message, callback) {
    if (callback != null) {
      callback(status, message)
    }
  }

  var request = function (method, data, url, onOk, onError) {
    var req = apiPrefix + url
    const getPromise = method(req, data)
    getPromise.then(response => {
      var obj = response.data
      if (obj.disclaimer !== undefined) {
        onOk(obj)
      }
      else {
        handleError(obj.status, obj.Message, onError)
      }
    }).catch(err => {
      handleError(-1, err, onError)
    })
    return getPromise
  }

  this.get = function (...args) {
    return request(Axios.get, null, ...args)
  }
}()
