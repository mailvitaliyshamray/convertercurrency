import moment from 'moment'

export default new function () {
  this.format = function (date) {
    return moment(date).format('DD-MM-YYYY')
  }
}()
