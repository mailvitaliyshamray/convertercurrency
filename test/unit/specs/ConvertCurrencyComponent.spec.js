import Vue from 'vue'
import ConvertCurrencyComponent from '@/components/ConvertCurrencyComponent'

describe('ConvertCurrencyComponent.vue', () => {
  let Constructor
  const Comp = ConvertCurrencyComponent

  function create () {
    return new Constructor().$mount()
  }

  beforeEach(() => {
    Constructor = Vue.extend(Comp)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('Should getRatesData will calls event onResetCalculatedData with param', () => {
    // Setup
    const vm = create()
    vm.$emit = jest.fn()
    const data = {
      from: [{ name: 'UAN', value: 27.7 }],
      to: [{ name: 'EUR', value: 0.86 }],
      amount: 100
    }
    const text = vm.$_calculateCurrency(data)

    // Act
    vm.getRatesData(data)

    // Assert
    vm.$nextTick(() => {
      expect(vm.$emit).toBeCalledWith('onResetCalculatedData', text)
      expect(vm.$emit.mock.calls.length).toEqual(1)
    })
  })

  it('Should $_calculateCurrency return a calculated result', () => {
    // Setup
    const vm = create()
    const data = {
      from: [{ name: 'UAN', value: 27.7 }],
      to: [{ name: 'EUR', value: 0.86 }],
      amount: 100
    }
    const text = 'You have converted 100 UAN to EUR  Вesign value: 3.10  EUR'

    // Act
    const result = vm.$_calculateCurrency(data)

    // Assert
    expect(result).toEqual(text)
  })

  it('Should $_separation return object', () => {
    // Setup
    const vm = create()
    const data = [{ name: 'UAN', value: 100 }]

    // Act
    const result = vm.$_separation(data)

    // Assert
    expect(result).toEqual({ name: 'UAN', value: 100 })
  })
})
