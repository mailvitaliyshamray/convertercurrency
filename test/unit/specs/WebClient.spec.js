import WebClient from '../../../src/services/WebClient'
import Axios from 'axios'

jest.mock('axios', () => {
  var requestMock = function (url, data) {
    if (url === '/api/paramErrorUrl') {
      return Promise.resolve({
        data: {
          Message: 'paramError',
          status: 403
        }
      })
    }
    else {
      return Promise.resolve({
        data: {
          disclaimer: {},
          Data: 'testData' + url + data
        }
      })
    }
  }
  return {
    get: jest.fn((url) => requestMock(url, ''))
  }
})

describe('WebClient.js', () => {
  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('should get', async () => {
    // Setup
    const testUrl = 'latest.json'
    const testData = 'testData'
    const testUrlFull = '/api/latest.json'
    var testResult = null

    // Act
    await WebClient.get(testUrl, message => {
      testResult = message.Data
    })

    // Assert  testData/api/url
    expect(testResult).toEqual(testData + testUrlFull)
  })

  it('should calback get param error', async () => {
    // Setup
    var testResult = null
    var testError = null
    var testStatus = null

    const testParamErrorUrl = 'paramErrorUrl'

    // Act
    await WebClient.get(testParamErrorUrl, message => {
      testResult = message
    },
      (status, error) => {
        testStatus = status
        testError = error
      })

    // Assert
    expect(testResult).toBeNull()
    expect(testStatus).toEqual(403)
    expect(testError).toEqual('paramError')
  })
})
