import Vue from 'vue'
import OpenExchange from '@/components/OpenExchange.vue'
import dateHelper from '../../../src//helpers/DateHelper'
import WebClient from '../../../src/services/WebClient'

jest.mock('../../../src/services/WebClient', () => ({
  get: jest.fn(function (ressource, callback) {
    callback(
      {
        rates: {
          'UAN': 27.7,
          'EUR': 0.86
        },
        timestamp: 1535378400
      }
    )
  })
}))

describe('OpenExchange.vue', () => {
  let Constructor
  const Comp = OpenExchange

  function create () {
    return new Constructor().$mount()
  }

  beforeEach(() => {
    Constructor = Vue.extend(Comp)
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('Should $_showCalculateDate will set text to resultOfConverted', () => {
    // Setup
    const vm = create()
    const converted = 'test calculate result'
    vm.resultOfConverted = null

    // Act
    vm.$_showCalculateDate(converted)

    // Assert
    expect(vm.resultOfConverted).toEqual(converted)
  })

  it('Should $_showCalculateDate calls $_resetCalculatedData', () => {
    // Setup
    const vm = create()
    const converted = 'test calculate result'
    vm.$_resetCalculatedData = jest.fn()

    // Act
    vm.$_showCalculateDate(converted)

    // Assert
    expect(vm.$_resetCalculatedData.mock.calls.length).toEqual(1)
    expect(vm.$_resetCalculatedData).toBeCalled()
  })

  it('Should $_resetCalculatedData return text', () => {
    // Setup
    const vm = create()
    vm.amount = 123
    vm.selecdedRateFrom = 'from'
    vm.selecdedRateTo = 'to'

    // Act
    vm.$_resetCalculatedData()

    // Assert
    expect(vm.selecdedRateFrom).toEqual('')
    expect(vm.selecdedRateTo).toEqual('')
    expect(vm.amount).toEqual(0)
  })

  it('Should $_haveDataFilled return true', () => {
    // Setup
    const vm = create()
    const data = {
      from: [{ name: 'from', value: 1.01 }],
      to: [{ name: 'to', value: 2.01 }],
      amount: 1000
    }

    // Act
    const result = vm.$_haveDataFilled(data)

    // Assert
    expect(result).toBeTruthy()
  })

  it('Should $_haveDataFilled return false', () => {
    // Setup
    const vm = create()
    const data = {
      amount: 1000,
      from: []
    }

    // Act
    const result = vm.$_haveDataFilled(data)

    // Assert
    expect(result).toBeFalsy()
  })

  it('Should $_haveDataFilled return false', () => {
    // Setup
    const vm = create()
    const data = {
      amount: 0
    }

    // Act
    const result = vm.$_haveDataFilled(data)

    // Assert
    expect(result).toBeFalsy()
  })

  it('Should $_convertCurrencyClick calls $_haveDataFilled', () => {
    // Setup
    const vm = create()
    vm.$_haveDataFilled = jest.fn()
    vm.selecdedRateFrom = 'UAN'
    vm.selecdedRateTo = 'EUR'
    vm.exchangeRates['rates'] = {
      'UAN': 27,
      'EUR': 0.86
    }

    // Act
    vm.$_convertCurrencyClick()

    // Assert
    expect(vm.$_haveDataFilled.mock.calls.length).toEqual(1)
    expect(vm.$_haveDataFilled).toBeCalled()
  })

  it('Should $_convertCurrencyClick calls $_сonvertСurrencyComponent.getRatesData with param', () => {
    // Setup
    const vm = create()
    const data = {
      from: [{ name: 'UAN', value: 27.7 }],
      to: [{ name: 'EUR', value: 0.86 }],
      amount: 100
    }
    vm.$_сonvertСurrencyComponent.getRatesData = jest.fn()
    vm.amount = 100
    vm.selecdedRateFrom = 'UAN'
    vm.selecdedRateTo = 'EUR'
    vm.exchangeRates = {
      rates: {
        'UAN': 27.7,
        'EUR': 0.86
      }
    }

    // Act
    vm.$_convertCurrencyClick()

    // Assert
    expect(vm.$_сonvertСurrencyComponent.getRatesData.mock.calls.length).toEqual(1)
    expect(vm.$_сonvertСurrencyComponent.getRatesData).toBeCalledWith(data)
  })

  it('Should $_convertCurrencyClick set null to resultOfConverted', () => {
    // Setup
    const vm = create()
    vm.resultOfConverted = 'text'

    // Act
    vm.$_convertCurrencyClick()

    // Assert
    expect(vm.resultOfConverted).toBeNull()
  })

  it('Should getTodayRate calls WebClient.get', async () => {
    // Setup
    const vm = create()
    const url = 'latest.json?app_id=74190d8a55834a4f8b5cc9709f6fc0af'
    // Act
    await vm.getTodayRate(url)

    // Assert
    expect(WebClient.get).toBeCalledWith(url, expect.anything(), expect.anything())
  })

  it('Should getTodayRate set data to exchangeRates', async () => {
    // Setup
    const vm = create()
    const url = 'latest.json?app_id=74190d8a55834a4f8b5cc9709f6fc0af'
    const testData = {
      rates: {
        'UAN': 27.7,
        'EUR': 0.86
      },
      timestamp: 1535378400
    }
    vm.exchangeRates = {}
    // Act
    await vm.getTodayRate(url)

    // Assert
    expect(vm.exchangeRates).toEqual(testData)
  })

  it('Should getTodayRate set date to today', async () => {
    // Setup
    const vm = create()
    const url = 'latest.json?app_id=74190d8a55834a4f8b5cc9709f6fc0af'
    const date = dateHelper.format(new Date(1535378400 * 1000))
    vm.exchangeRates = {}
    // Act
    await vm.getTodayRate(url)

    // Assert
    expect(vm.today).toEqual(date)
  })
})
